#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <stdbool.h>
#include <assert.h>
#include <pthread.h>
#include <inttypes.h>

#include "matrix.h"

static uint32_t g_seed = 0;

static ssize_t g_width = 0;
static ssize_t g_height = 0;
static ssize_t g_elements = 0;
static ssize_t g_nthreads = 1;
static ssize_t chunk = 1;
static ssize_t chunk_max = 1;
static pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;

typedef struct argument_mult {
    const uint32_t *a; //matrix a for multiplication
    const uint32_t *b; //matrix b
    uint32_t *storage; //store result
    size_t thread_id;
} argument_mult;

 typedef struct padded_int
    {
    uint32_t val;
    int pad[64-sizeof(uint32_t)]; //prevent false sharing
    } padded_int;

typedef struct argument_max {
    const uint32_t *matrix; //matrix to check
    padded_int *private_max; //element of array that stores maximum for each thread
    uint32_t *max; //variable to store actual maximum
    size_t thread_id;
    
  
} argument_max;

typedef struct argument_freq {
    const uint32_t *matrix; //matrix to check
    padded_int *private_freq; //element of array that stores frequency for each thread
    uint32_t *frequency; //variable to store actual frequency
    uint32_t value; //value to check
    size_t thread_id;
  
} argument_freq;

typedef struct argument_scalar {
    const uint32_t *matrix; //matrix to check
     uint32_t *newmatrix;
   uint32_t scalar; //variable to store actual sum
    size_t thread_id;
    
  
} argument_scalar;


////////////////////////////////
///     UTILITY FUNCTIONS    ///
////////////////////////////////

/**
 * Returns pseudorandom number determined by the seed
 */
uint32_t fast_rand(void) {

    g_seed = (214013 * g_seed + 2531011);
    return (g_seed >> 16) & 0x7FFF;
}

/**
 * Sets the seed used when generating pseudorandom numbers
 */
void set_seed(uint32_t seed) {

    g_seed = seed;
}

/**
 * Sets the number of threads available
 */
void set_nthreads(ssize_t count) {

    g_nthreads = count;
}

/**
 * Sets the dimensions of the matrix
 */
void set_dimensions(ssize_t order) {

    g_width = order;
    g_height = order;

    g_elements = g_width * g_height;
    chunk = g_width / g_nthreads;
    chunk_max = g_elements / g_nthreads;
}

/**
 * Displays given matrix
 */
void display(const uint32_t* matrix) {

    for (ssize_t y = 0; y < g_height; y++) {
        for (ssize_t x = 0; x < g_width; x++) {
            if (x > 0) printf(" ");
            printf("%" PRIu32, matrix[y * g_width + x]);
        }

        printf("\n");
    }
}

/**
 * Displays given matrix row
 */
void display_row(const uint32_t* matrix, ssize_t row) {

    for (ssize_t x = 0; x < g_width; x++) {
        if (x > 0) printf(" ");
        printf("%" PRIu32, matrix[row * g_width + x]);
    }

    printf("\n");
}

/**
 * Displays given matrix column
 */
void display_column(const uint32_t* matrix, ssize_t column) {

    for (ssize_t y = 0; y < g_height; y++) {
        printf("%" PRIu32 "\n", matrix[y * g_width + column]);
    }
}

/**
 * Displays the value stored at the given element index
 */
void display_element(const uint32_t* matrix, ssize_t row, ssize_t column) {

    printf("%" PRIu32 "\n", matrix[row * g_width + column]);
}

////////////////////////////////
///   MATRIX INITALISATIONS  ///
////////////////////////////////

/**
 * Returns new matrix with all elements set to zero
 */
uint32_t* new_matrix(void) {

    return calloc(g_elements, sizeof(uint32_t));
}

/**
 * Returns new identity matrix
 */
uint32_t* identity_matrix(void) {

    uint32_t* matrix = new_matrix();

    for (ssize_t i = 0; i < g_width; i++) {
        matrix[i * g_width + i] = 1;
    }

    return matrix;
}

/**
 * Returns new matrix with elements generated at random using given seed
 */
uint32_t* random_matrix(uint32_t seed) {

    uint32_t* matrix = new_matrix();

    set_seed(seed);

    for (ssize_t i = 0; i < g_elements; i++) {
        matrix[i] = fast_rand();
    }

    return matrix;
}

/**
 * Returns new matrix with all elements set to given value
 */
uint32_t* uniform_matrix(uint32_t value) {

    uint32_t* matrix = new_matrix();

    for (ssize_t i = 0; i < g_elements; i++) {
        matrix[i] = value;
    }

    return matrix;
}

/**
 * Returns new matrix with elements in sequence from given start and step
 */
uint32_t* sequence_matrix(uint32_t start, uint32_t step) {

    uint32_t* matrix = new_matrix();
    uint32_t current = start;

    for (ssize_t i = 0; i < g_elements; i++) {
        matrix[i] = current;
        current += step;
    }

    return matrix;
}

////////////////////////////////
///     MATRIX OPERATIONS    ///
////////////////////////////////

/**
 * Returns new matrix with elements cloned from given matrix
 */
uint32_t* cloned(const uint32_t* matrix) {

    uint32_t* result = new_matrix();

    for (ssize_t i = 0; i < g_elements; i++) {
        result[i] = matrix[i];
    }

    return result;
}

/**
 * Returns new matrix with elements ordered in reverse
 */
uint32_t* reversed(const uint32_t* matrix) {

    uint32_t* result = new_matrix();

    for (ssize_t i = 0; i < g_elements; i++) {
        result[i] = matrix[g_elements - 1 - i];
    }

    return result;
}

/**
 * Returns new transposed matrix
 */
uint32_t* transposed(const uint32_t* matrix) {

    uint32_t* result = new_matrix();

    for (ssize_t y = 0; y < g_height; y++) {
        for (ssize_t x = 0; x < g_width; x++) {
            result[x * g_width + y] = matrix[y * g_width + x];
        }
    }

    return result;
}


/**
 * Returns a matrix with a number added to each element
 */
uint32_t* scalar_add(const uint32_t* matrix, uint32_t scalar) {
    
    uint32_t* newmatrix = new_matrix();
   
    // create the jobs for each thread
    argument_scalar *args = malloc(sizeof(argument_scalar) * g_nthreads);
    if (!args) {
        perror("malloc");
        exit(1);
    }
    
    for (size_t i = 0; i < g_nthreads; ++i) {
        args[i] = (argument_scalar) {
            .newmatrix = newmatrix,
            .matrix = matrix,
            .scalar = scalar,
            .thread_id = i,
         };
    }

    // stores the thread IDs
    pthread_t thread_ids[g_nthreads];

    // launch
    for (size_t i = 0; i < g_nthreads; ++i) {
        if (pthread_create(thread_ids + i, NULL, add_worker, args + i) != 0) 
        {
            perror("pthread_create failed");
            exit(1);
        }
    }

    // wait for the all the threads to finish
    for (size_t i = 0; i < g_nthreads; ++i) {
        if (pthread_join(thread_ids[i], NULL) != 0) 
        {
            perror("pthread_join failed");
            exit(1);
        }
    }

    free(args);
    return newmatrix;
}


/**
* Worker for scalar add
*/

void *add_worker(void *args)
{
    argument_scalar* arg = (argument_scalar*)args; //arguments array
    
    const uint32_t *matr = arg->matrix;
    uint32_t *newmatrix = arg->newmatrix;
    
     const size_t start = arg->thread_id * chunk_max;
    
     const size_t end = 
            arg->thread_id == (g_nthreads - 1) ? (g_elements-1) : (start + chunk_max-1);

    for (ssize_t i = start; i <= end; i++)
    {
        {
            newmatrix[i] = matr[i] + arg->scalar;
        
        }
    }
    
    return NULL;
}
    

uint32_t* scalar_mul(const uint32_t* matrix, uint32_t scalar) {
    
    uint32_t* newmatrix = new_matrix();
   
    // create the jobs for each thread
    argument_scalar *args = malloc(sizeof(argument_scalar) * g_nthreads);
    if (!args) {
        perror("malloc");
        exit(1);
    }
    
    for (size_t i = 0; i < g_nthreads; ++i) {
        args[i] = (argument_scalar) {
            .newmatrix = newmatrix,
            .matrix = matrix,
            .scalar = scalar,
            .thread_id = i,
         };
    }

    // stores the thread IDs
    pthread_t thread_ids[g_nthreads];

    // launch
    for (size_t i = 0; i < g_nthreads; ++i) {
        if (pthread_create(thread_ids + i, NULL, scalar_mul_worker, args + i) != 0) 
        {
            perror("pthread_create failed");
            exit(1);
        }
    }

    // wait for the all the threads to finish
    for (size_t i = 0; i < g_nthreads; ++i) {
        if (pthread_join(thread_ids[i], NULL) != 0) 
        {
            perror("pthread_join failed");
            exit(1);
        }
    }

    free(args);
    return newmatrix;
}


/**
* Worker for scalar multiplication
*/

void *scalar_mul_worker(void *args)
{
    argument_scalar* arg = (argument_scalar*)args; //arguments array
    
    const uint32_t *matr = arg->matrix;
    uint32_t *newmatrix = arg->newmatrix;
    
     const size_t start = arg->thread_id * chunk_max;
    
     const size_t end = 
            arg->thread_id == (g_nthreads - 1) ? (g_elements-1) : (start + chunk_max-1);

    for (ssize_t i = start; i <= end; i++)
    {
        {
            newmatrix[i] = matr[i] * arg->scalar;
        
        }
    }
    
    return NULL;
}


/**
 * Returns new matrix with scalar multiplied to each element
 
uint32_t* scalar_mul(const uint32_t* matrix, uint32_t scalar) {

    uint32_t* result = new_matrix();

    for (ssize_t i = 0; i < g_elements; i++) 
    {
        result[i] = (matrix[i]*scalar);
    }


    return result;
}*/

/**
 * Returns new matrix with elements added at the same index
 */
uint32_t* matrix_add(const uint32_t* matrix_a, const uint32_t* matrix_b) {

    uint32_t* result = new_matrix();

    for (ssize_t i = 0; i < g_elements; i++) 
    {
        result[i] = (matrix_a[i]+matrix_b[i]);
    }

    return result;
}



/**
 * Returns new matrix, multiplying the two matrices together
 
uint32_t* matrix_mul(const uint32_t* matrix_a, const uint32_t* matrix_b) {

    uint32_t* transpose = new_matrix();
    uint32_t* result = new_matrix();

    //transpose the matrix b to reduce memory access
    
     for (ssize_t i = 0; i < g_width; i++) 
    {
        for (ssize_t j = 0; j < g_width; j++)
        {
            transpose[i*g_width + j] = matrix_b[j*g_width + i];
        }
     }
            
            
    
    for (ssize_t i = 0; i < g_width; i++) 
    {
        for (ssize_t j = 0; j < g_width; j++)
        {
            uint32_t sum = 0;
            
            for (ssize_t k = 0; k < g_height; k++)
            {
                sum += ((matrix_a[(i * g_height + k)]) * (transpose[( j * g_height + k)]));
            }
            
            result[j + i*g_height]=sum;
        }
    }

    free(transpose);
    return result;
}*/

uint32_t* matrix_mul(const uint32_t* matrix_a, const uint32_t* matrix_b) {
    
    //uint32_t* transpose = new_matrix();
    uint32_t* result = new_matrix();
    
    /* //transpose the matrix b to reduce memory access
    
     for (ssize_t i = 0; i < g_width; i++) 
    {
        for (ssize_t j = 0; j < g_width; j++)
        {
            transpose[i*g_width + j] = matrix_b[j*g_width + i];
        }
     } */
    
    // create the jobs for each thread
    argument_mult *args = malloc(sizeof(argument_mult) * g_nthreads);
    if (!args) {
        perror("malloc");
        exit(1);
    }

    for (size_t i = 0; i < g_nthreads; ++i) {
        args[i] = (argument_mult) {
            .a = matrix_a,
            .b = matrix_b,
            .storage = result,
            .thread_id = i,
        };
    }

    // stores the thread IDs
    pthread_t thread_ids[g_nthreads];

    // launch
    for (size_t i = 0; i < g_nthreads; ++i) {
        if (pthread_create(thread_ids + i, NULL, mul_worker, args + i) != 0) 
        {
            perror("pthread_create failed");
            exit(1);
        }
    }

    // wait for the all the threads to finish
    for (size_t i = 0; i < g_nthreads; ++i) {
        if (pthread_join(thread_ids[i], NULL) != 0) 
        {
            perror("pthread_join failed");
            exit(1);
        }
    }

    free(args);
  
    //free(transpose);
    return result;
}

 void *mul_worker(void *args) {
     
    argument_mult arg = *(argument_mult *)args;

    // sum the values from start to end
    const size_t start = arg.thread_id * chunk;
     
     const size_t end = 
         arg.thread_id == g_nthreads - 1 ? g_width : (arg.thread_id + 1) * chunk;

    /*for (size_t y = start; y < end; ++y) {
        for (size_t x = 0; x < WIDTH; x++) {
            for (size_t k = 0; k < WIDTH; k++) {
                arg.res[IDX(x, y)] += arg.a[IDX(k, y)] * arg.b[IDX(x, k)];
            }
        }
    }*/
     
     for (ssize_t y = start; y < end; ++y) 
    {
        for (ssize_t x = 0; x < g_width; ++x)
        {
            uint32_t sum = 0;
            
            for (ssize_t k = 0; k < g_width; k++)
            {
            sum += ((arg.a[(y * g_width + k)]) * (arg.b[( k * g_width + x)]));
            }
            
            arg.storage[y*g_width + x] = sum;

        }
    }
    
     
    return NULL;
}


/**
 * Returns new matrix, powering the matrix to the exponent
 */
uint32_t* matrix_pow(const uint32_t* matrix, uint32_t exponent) {

  uint32_t* storage = identity_matrix();
    uint32_t* matr = (uint32_t*)matrix;
    uint32_t t;
    uint32_t exp = exponent;
    
    while (1)
    {
        t = exp % 2;
        exp = (uint32_t)(exp/2);
        
        if (t ==1)
        {
            storage = matrix_mul(storage, matr);
        }
        
        if (exp == 0)
            break;
        
        matr = matrix_mul(matr, matr);
        
    }
    return storage;
    
     
    /* if (exponent == 0)
    {
        return identity_matrix();
        
    }
    
    else
    {
        for (int i = 0; i<g_elements; i++)
        {
            storage[i] = matrix[i];
            }
        
        for (int i = 1; i<exponent; i++)
        {
            storage = matrix_mul(storage, matrix);
        }
        
        return storage;
    }*/         
}

////////////////////////////////
///       COMPUTATIONS       ///
////////////////////////////////

/**
 * Returns the sum of all elements
 */
uint32_t get_sum(const uint32_t* matrix) {

    uint32_t result = 0;
    for (ssize_t i = 0; i < g_elements; i++)
    {
        result += matrix[i];
    }
    
    return result;
}

/**
 * Returns the trace of the matrix
 */
uint32_t get_trace(const uint32_t* matrix) {

    uint32_t sum = 0;
    
    for (ssize_t i = 0; i <g_height; i++) 
    {
            sum += matrix[(i*g_height + i)];

    }
    return sum;
}

/**
 * Returns the smallest value in the matrix
 */
uint32_t get_minimum(const uint32_t* matrix) {

   uint32_t min = matrix[0]; // so 'min' isn't smaller than every element in the matrix
    
    for (ssize_t i = 1; i < g_elements; i++) //can start from one because of above assignment
    {
        if (min > matrix[i])
            min = matrix[i];
    }
    
    return min;
}

/**
 * Returns the largest value in the matrix
 
uint32_t get_maximum(const uint32_t* matrix) {

     uint32_t max = 0; 
    
    for (ssize_t i = 0; i < g_elements; i++)
    {
        if (max < matrix[i])
            max = matrix[i];
    }
    
    return max;
}*/

/**
 * Returns the largest value in the matrix PARALLEL
 */
uint32_t get_maximum(const uint32_t* matrix) {
   
    padded_int private_max[g_nthreads];
     uint32_t max = 0; 
   
    
    
    // create the jobs for each thread
    argument_max *args = malloc(sizeof(argument_max) * g_nthreads);
    if (!args) {
        perror("malloc");
        exit(1);
    }

    for (size_t i = 0; i < g_nthreads; ++i) {
        args[i] = (argument_max) {
            .matrix = matrix,
            .private_max = &private_max[i],
            .max = &max,
            .thread_id = i
         };
        args[i].private_max->val =0;
    }

    // stores the thread IDs
    pthread_t thread_ids[g_nthreads];

    // launch
    for (size_t i = 0; i < g_nthreads; ++i) {
        if (pthread_create(thread_ids + i, NULL, max_worker, args + i) != 0) 
        {
            perror("pthread_create failed");
            exit(1);
        }
    }

    // wait for the all the threads to finish
    for (size_t i = 0; i < g_nthreads; ++i) {
        if (pthread_join(thread_ids[i], NULL) != 0) 
        {
            perror("pthread_join failed");
            exit(1);
        }
    }

    free(args);
    
    return max;
}


/**
* Worker for get_maximum
*/

void *max_worker(void *args)
    
    //void *matrix, void *private_max, void *max
{
    argument_max* arg = (argument_max*)args; //arguments array
    
    uint32_t *maxi = arg->max; //pointer to global max variable
    const uint32_t *matr = arg->matrix;
     padded_int *private = arg->private_max;
    
   
     const size_t start = arg->thread_id * chunk_max;
    
     const size_t end = 
            arg->thread_id == (g_nthreads - 1) ? (g_elements-1) : (start + chunk_max-1);

    for (ssize_t i = start; i <= end; i++)
    {
        if (private->val < matr[i])
        {
            private->val = matr[i];
        
        }
    }
    
    pthread_mutex_lock(&m);
    if (*maxi < private->val)
    {
    
        *maxi = private->val;
        
    }
    pthread_mutex_unlock(&m);
    return NULL;
}
    


/**
 * Returns the frequency of the value in the matrix

uint32_t get_frequency(const uint32_t* matrix, uint32_t value) {

    uint32_t count = 0; 
    
    for (ssize_t i = 0; i < g_elements; i++)
    {
        if (matrix[i] == value)
            count++;
    }
    
    return count;
}

 */

/**
 * Returns the frequency of the given value in the matrix
 */
uint32_t get_frequency(const uint32_t* matrix, uint32_t value) {
   
    padded_int private_freq[g_nthreads]; //store result for each thread
    
    for (uint32_t i = 0; i<g_nthreads; i++)
    {
        private_freq[i].val = 0;
    }
    
     uint32_t frequency = 0; 
   
    
    
    // create the jobs for each thread
    argument_freq *args = malloc(sizeof(argument_freq) * g_nthreads);
    if (!args) {
        perror("malloc");
        exit(1);
    }

    for (size_t i = 0; i < g_nthreads; ++i) {
        args[i] = (argument_freq) {
            .matrix = matrix, 
            .private_freq = &private_freq[i],
            .value = value, //value to check
            .frequency = &frequency, //frequency variable
            .thread_id = i
         };
        
    }

    // stores the thread IDs
    pthread_t thread_ids[g_nthreads];

    // launch
    for (size_t i = 0; i < g_nthreads; ++i) {
        if (pthread_create(thread_ids + i, NULL, freq_worker, args + i) != 0) 
        {
            perror("pthread_create failed");
            exit(1);
        }
    }

    // wait for the all the threads to finish
    for (size_t i = 0; i < g_nthreads; ++i) {
        if (pthread_join(thread_ids[i], NULL) != 0) 
        {
            perror("pthread_join failed");
            exit(1);
        }
    }

    free(args);
    
    return frequency;
}


/**
* Worker for get_frequency
*/

void *freq_worker(void *args)
    
    //void *matrix, void *private_max, void *max
{
    argument_freq* arg = (argument_freq*)args; //arguments array
    
    uint32_t *frequency = arg->frequency; //pointer to global freq variable
    const uint32_t *matr = arg->matrix; //matrix to check
     padded_int *private = arg->private_freq;
    uint32_t value = arg->value;
   
    
   
     const size_t start = arg->thread_id * chunk_max;
    
     const size_t end = 
            arg->thread_id == (g_nthreads - 1) ? (g_elements-1) : (start + chunk_max-1);

    for (ssize_t i = start; i <= end; i++)
    {
       
            if (matr[i] == value)
            {
            private->val+=1;
            }
        
    }
    
    pthread_mutex_lock(&m);
    *frequency += private->val;
    pthread_mutex_unlock(&m);
    return NULL;
}
    

